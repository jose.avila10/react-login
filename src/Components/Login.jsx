import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router';
import axios from 'axios';
import CryptoAES from 'crypto-js/aes';

import LoginForm from './LoginForm';
import Loading from './Loading';

const encryptatonKey = "-KaPdSgVkYp2s5v8y/B?E(H+MbQeThWmZq4t6w9z$C&F)J@NcRfUjXn2r5u8x!A%D*G-KaPdSgVkYp3s6v9y$B?E(H+MbQeThWmZq4t7w!z%C*F)J@NcRfUjXn2r5u8x/A?D(G+KaPdSgVkYp3s6v9y$B&E)H@McQfThWmZq4t7w!z%C*F-JaNdRgUkXn2r5u8x/A?D(G+KbPeShVmYq3s6v9y$B&E)H@McQfTjWnZr4u7w!z%C*F-JaNdRgUkXp2s5v8y/A?D(G+KbPeShVmYq3t6w9z$C&E)H@McQfTjWnZr4u7x!A%D*G-JaNdRgUkXp2s5v8y/B?E(H+MbPeShVmYq3t6w9z$C&F)J@NcRfTjWnZr4u7x!A%D*G-KaPdSgVkXp2s5v8y/B?E(H+MbQeThWmZq3t6w9z$C&F)J@NcRfUjXn2r5u7x!A%D*G-KaPdSgVkYp3s6v9y/B?E(H+MbQeThWmZq4t7w!z%C&F)J@NcRfUjXn2r5u8x/A?D(";

const Login = () => {
    const navigate = useNavigate();

    function setCookie(name, value, days) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days*24*60*60*1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + (value || "")  + expires + "; path=/";
    }

    const readCookie = (cookieName) => {
        var nameEQ = cookieName + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)===' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }

    const deleteCookie = (name) => {
        document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }

    deleteCookie('Bearer'); //To make sure Bearer token is deleted when sign in

    const [loadingStatus, setLoadingStatus] = useState(true);
    const [loginDataState, setLoginDataState] = useState([]);

    const validationRequest = (dataToLogin, remember) => {
        setLoadingStatus(true);
        
        axios({
            method: 'post',
            url: 'http://localhost:3001/login',
            data: dataToLogin
        })
        .then(res => {
            setLoginDataState(res.data);
            setCookie('Bearer', res.data.access_token, 0.000695); //Save the cookie for 60 seconds
            if(remember){
                const encUser = CryptoAES.encrypt(dataToLogin.username, encryptatonKey).toString();
                const encPassword = CryptoAES.encrypt(dataToLogin.password, encryptatonKey).toString();
                setCookie('usernameReactPractice', encUser, 365); //Save the cookie for one year or untill overwrite
                setCookie('passwordReactPractice', encPassword, 356); //Save the cookie for one year or untill overwrite
                
            }
            navigate('/');
        })
        .catch(err => {
            document.cookie = "Bearer=";
            setLoginDataState(err.response.data);
        })

        setLoadingStatus(false);
    }

    useEffect(() => { //To handle when invalid login credentials has been submitted
        if(loginDataState.length !== 0){
            if(loginDataState.statusCode === 401){
                alert("Invalid credentials. Either username or password maybe wrong")
            }
        }
    }, [loginDataState])


    useEffect(() =>{ //To read if there are username and password cookies stores
        const userSaved = readCookie('usernameReactPractice');
        const passwordSaved = readCookie('passwordReactPractice');
        if(userSaved !== null && passwordSaved !== null){
            navigate('/');
        } else {
            setLoadingStatus(false);
        }
    }, [navigate])

    return(
        <div>
            <div className="w-50 mx-auto my-2">
                {loadingStatus &&
                    <Loading />
                }

                {!loadingStatus && 
                    <LoginForm validate={validationRequest}/>
                }
            </div>
        </div>
    )
}

export default Login;