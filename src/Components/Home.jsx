import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router';
import axios from 'axios';
import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

import Loading from './Loading';

const encryptatonKey = "-KaPdSgVkYp2s5v8y/B?E(H+MbQeThWmZq4t6w9z$C&F)J@NcRfUjXn2r5u8x!A%D*G-KaPdSgVkYp3s6v9y$B?E(H+MbQeThWmZq4t7w!z%C*F)J@NcRfUjXn2r5u8x/A?D(G+KaPdSgVkYp3s6v9y$B&E)H@McQfThWmZq4t7w!z%C*F-JaNdRgUkXn2r5u8x/A?D(G+KbPeShVmYq3s6v9y$B&E)H@McQfTjWnZr4u7w!z%C*F-JaNdRgUkXp2s5v8y/A?D(G+KbPeShVmYq3t6w9z$C&E)H@McQfTjWnZr4u7x!A%D*G-JaNdRgUkXp2s5v8y/B?E(H+MbPeShVmYq3t6w9z$C&F)J@NcRfTjWnZr4u7x!A%D*G-KaPdSgVkXp2s5v8y/B?E(H+MbQeThWmZq3t6w9z$C&F)J@NcRfUjXn2r5u7x!A%D*G-KaPdSgVkYp3s6v9y/B?E(H+MbQeThWmZq4t7w!z%C&F)J@NcRfUjXn2r5u8x/A?D(";

const Logged = () => {
    const navigate = useNavigate();

    const [loadingStatus, setLoadingStatus] = useState(false);
    const [authResponse, setAuthResponse] = useState([]);
    const [dataOnCookies, setDataOnCookies] = useState(true);

    function setCookie(name, value, days) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days*24*60*60*1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + (value || "")  + expires + "; path=/";
    }

    const readCookie = (cookieName) => {
        var nameEQ = cookieName + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)===' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }

    const deleteCookie = (name) => {
        document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }

    useEffect(() => { //Trying to acces '/protected' on the server, using current auth_token
        setLoadingStatus(true);

        let auth_token = readCookie('Bearer');

        axios.get('http://localhost:3001/protected', { headers: {'Authorization': 'Bearer ' + auth_token} })
        .then(res => setAuthResponse(res.data))
        .catch(err => setAuthResponse(err.response.data))

        .then(setLoadingStatus(false))
    }, []);

    useEffect(() =>{ //To read if there are username and password cookies stores and relog automaticly
        const currentToken = readCookie('Bearer');

        const userSaved = readCookie('usernameReactPractice');
        const passwordSaved = readCookie('passwordReactPractice');

        if(userSaved !== null && passwordSaved !== null){
            const desEncUser = CryptoAES.decrypt(userSaved, encryptatonKey).toString(CryptoENC);
            const desEncPassword = CryptoAES.decrypt(passwordSaved, encryptatonKey).toString(CryptoENC);

            const dataRemembered = {
                "username": desEncUser,
                "password": desEncPassword
            }
            if(currentToken === null){
                setLoadingStatus(true);

                axios({
                    method: 'post',
                    url: 'http://localhost:3001/login',
                    data: dataRemembered
                })
                .then(res => {
                    setCookie('Bearer', res.data.access_token, 0.000695); //Save the cookie for 60 seconds
                    window.location.reload();
                })

                setLoadingStatus(false);
            }
            
        } else {
            setDataOnCookies(false);
        }
    }, [])


    const logout = () => {
        deleteCookie('usernameReactPractice');
        deleteCookie('passwordReactPractice');

        navigate('/login');
    }

    return (
        <div>
            {loadingStatus &&
                <Loading />
            }

            {dataOnCookies && authResponse.statusCode === 401 &&
                <div>
                    <Loading />
                </div>
            }

            {!loadingStatus && authResponse.statusCode === 401 && !dataOnCookies &&
                <div>
                    Please, <a href="/login">login</a> to have access to this platform
                </div>
            }

            {!loadingStatus && authResponse.name &&
                <div>
                    Welcome {authResponse.name}, Now you can have access to this platform.
                    <button type="button" className="btn btn-danger mx-2" onClick={logout}>Logout</button>
                </div>
            }
        </div>
    )
}

export default Logged;