import React, { useState } from 'react';
import './styles/LoginForm.css';

const LoginForm = (props) => {
    const [usernameInput, setUsernameInput] = useState('');
    const [passwordInput, setPasswordInput] = useState('');
    const [rememberCheck, setrememberCheck] = useState(false);

    const handleUsernameChange = (event) => {
        setUsernameInput(event.target.value);
    }

    const handlePasswordChange = (event) => {
        setPasswordInput(event.target.value);
    }

    const handleRememberChange = () => {
        setrememberCheck(!rememberCheck);
    }

    const loginSubmit = () => {
        if(usernameInput !== "" && passwordInput !== ""){
            const infoToLogin = {
                "username": usernameInput,
                "password": passwordInput
            }
            props.validate(infoToLogin, rememberCheck);
        } else {
            alert("Please, Don't let any empty field");
        }
    }

    return(
        <div className="border border-dark rounded">
            <div className="header-form">
                Welcome to React Login Practice
            </div>

            <div className="form-group p-2">
                <label>Username</label>
                <input type="text" className="form-control" value={usernameInput} onChange={handleUsernameChange}/>
                <br />
                <label>Username</label>
                <input type="password" className="form-control" value={passwordInput} onChange={handlePasswordChange}/>
                <input type="checkbox" className="form-check-input" value={rememberCheck} onChange={handleRememberChange} />
                <label className="form-check-label mx-2">Remember me</label>
                <br /><br />
                <button type="button" className="btn btn-primary" onClick={loginSubmit}>Login</button>
            </div>
        </div>
    )
}

export default LoginForm;